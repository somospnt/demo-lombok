package com.somospnt.demolombok.service;

import com.somospnt.demolombok.domain.Persona;

import java.util.List;

public interface PersonaService {

    List<Persona> obtenerTodas();

    Persona guardar(Persona persona);
}
