package com.somospnt.demolombok.service.impl;

import com.somospnt.demolombok.domain.Persona;
import com.somospnt.demolombok.repository.PersonaRepository;
import com.somospnt.demolombok.service.PersonaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PersonaServiceImpl implements PersonaService {

    private final PersonaRepository personaRepository;

    @Override
    public List<Persona> obtenerTodas() {
        return personaRepository.findAll();
    }

    @Override
    public Persona guardar(Persona persona) {
        return personaRepository.save(persona);
    }
}
