package com.somospnt.demolombok.repository;

import com.somospnt.demolombok.domain.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<Persona, Long> {
}
