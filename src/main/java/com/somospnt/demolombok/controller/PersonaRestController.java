package com.somospnt.demolombok.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.somospnt.demolombok.domain.Persona;
import com.somospnt.demolombok.service.PersonaService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("personas")
public class PersonaRestController {

    private final PersonaService personaService;
    private final ObjectMapper objectMapper;

    @GetMapping
    public List<Persona> obtenerTodas() {
        log.info("Obteniendo personas");
        val personas = personaService.obtenerTodas();
        personas.forEach(System.out::println);

        return personas;
    }

    @PostMapping
    @SneakyThrows
    public Persona crearDesdeJson() {
        String json = "{\"nombre\" : \"el maestruli\", \"edad\" : 40}";
        Persona persona = objectMapper.readValue(json, Persona.class);

        return personaService.guardar(persona);
    }

}
