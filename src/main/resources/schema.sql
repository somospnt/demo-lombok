DROP TABLE IF EXISTS persona;

CREATE TABLE persona(
    id  BIGINT PRIMARY KEY IDENTITY,
    nombre VARCHAR(50) NOT NULL,
    edad INTEGER NOT NULL
);

INSERT INTO persona
(nombre, edad)
VALUES
('Miller', 50),
('Viejo de las pastas', 80),
('Majul', 55);